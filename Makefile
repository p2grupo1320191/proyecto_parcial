
bin/prueba: obj/prueba.o obj/programa.o obj/juego.o obj/documento.o 
	gcc obj/prueba.o obj/programa.o obj/juego.o obj/documento.o -o bin/prueba

obj/prueba.o: src/prueba.c
	gcc -Wall -c -I include/ src/prueba.c -o obj/prueba.o

obj/programa.o: src/programa.c
	gcc -Wall -c -I include/ src/programa.c -o obj/programa.o

obj/juego.o: src/juego.c
	gcc -Wall -c -I include/ src/juego.c -o obj/juego.o

obj/documento.o: src/documento.c
	gcc -Wall -c -I include/ src/documento.c -o obj/documento.o

.PHONY: clean
clean:
	rm bin/* obj/*.o


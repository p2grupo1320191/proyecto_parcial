//En este archivo ud podra definir los objetos que podrán ser usados 
//con el slab allocator. Declare tambien el constructor y destructor de 
//cada objeto declarado. Abajo se provee un objeto de ejemplo.
//Declare al menos tres objetos distintos

//El constructor siempre recibe un puntero tipo void al tipo de objeto,
//y su tamaño (tal como se declaro el puntero a funcion).
//No olvide implementar las funciones de crear y destruir de cada objeto.

typedef struct ejemplo{
    int a;
    float b[100];
    char *msg;
    unsigned int refcount;
} Ejemplo; 

typedef struct juego{
	char *nombreJ;
	char *descripcionJ;
	int tamanioJ;
	int niveles;
	char *msg;
} Juego;

typedef struct documento{
	char *nombreD;
	char *descripcionD;
	char *extension;
	char *fecha_modif;
	int tamanioD;
	char *msg;
} Documento;

typedef struct programa{
	char *nombreP;
	char *descripcionP;
	int tamanioP;
	char *msg;
} Programa;



//Constructor
void crear_Ejemplo(void *ref, size_t tamano);

//Destructor
void destruir_Ejemplo(void *ref, size_t tamano);

//TODO: Crear 3 objetos mas

#include <unistd.h>
#include <stdlib.h>
#include "objetos.h"

void crear_Juego(void *ref, size_t tam){
        Juego *jue=(Juego *)ref;
        jue->nombreJ = "JuegoOnline";
        jue->tamanioJ = 1000;
	jue->niveles = 10;
        jue->descripcionJ = "te vas a divertir";
        jue->msg = (char *)malloc(sizeof(char)*1000);
}

void destruir_Juego(void *ref, size_t tam){
        Juego  *jue = (Juego *)ref;
        jue->nombreJ = NULL;
        jue->tamanioJ = 0;
	jue->niveles = 0;
        jue->descripcionJ = NULL;
        free(jue ->msg);
}


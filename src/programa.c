
#include <unistd.h>
#include <stdlib.h>
#include "objetos.h"

void crear_Programa(void *ref, size_t tam){
        Programa *prog=(Programa *)ref;
        prog->nombreP = "editor de texto";
        prog->tamanioP = 1000;
        prog->descripcionP = "esto es una descripcion";
        prog->msg = (char *)malloc(sizeof(char)*1000);
}

void destruir_Programa(void *ref, size_t tam){
        Programa  *prog = (Programa *)ref;
        prog->nombreP = NULL;
        prog->tamanioP = 0;
        prog->descripcionP = NULL;
        free(prog ->msg);
}



#include <unistd.h>
#include <stdlib.h>
#include "objetos.h"

void crear_Documento(void *ref, size_t tam){
	Documento *doc=(Documento *)ref;
	doc->nombreD = "nombreDocumento";
	doc->tamanioD = 1000;
	doc->descripcionD = "esto es una descripcion";
	doc->extension = "extension";
	doc->fecha_modif = "6/28/2019";
	doc->msg = (char *)malloc(sizeof(char)*1000);
}

void destruir_Documento(void *ref, size_t tam){
	Documento  *doc = (Documento *)ref;
	doc->nombreD = NULL;
        doc->tamanioD = 0;
        doc->descripcionD = NULL;
	doc->extension = NULL;
        doc->fecha_modif = NULL;
	free(doc ->msg);

}
